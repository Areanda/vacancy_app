import os
from celery import Celery
from email_lib import EmailWrapper
from models import EmailCredentials
import al_db

RABBIT_HOST = os.environ.get('RABBIT_HOST', 'localhost')
app = Celery('celery_worker', broker=f'pyamqp://guest@{RABBIT_HOST}//')


@app.task
def send_email(id_email_creds, recipient, message, subject):
    al_db.init_db()
    email_creds_details = al_db.db_session.query(EmailCredentials).get(id_email_creds)
    email_wrapper = EmailWrapper(**email_creds_details.get_mandatory_fields())
    email_wrapper.send_email(recipient, message, subject)




