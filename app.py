from flask import Flask
from flask import request, render_template, redirect, flash, url_for
import al_db
import mongo_db
from bson.objectid import ObjectId
import email_lib
from models import Vacancy, Event, EmailCredentials, User
from celery_worker import send_email
from flask import session


app = Flask(__name__)
app.secret_key = 'Password123456789'


@app.route("/", methods=['GET'])
def main_page():
    current_user = session.get('user_name', None)
    return render_template('main_page.html', user_name=current_user)


@app.route('/registration/', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        user_name = request.form.get('name')
        user_login = request.form.get('login')
        user_password = request.form.get('password')
        email = request.form.get('email')
        new_user = User(name=user_name, login=user_login, password=user_password, email=email)
        al_db.db_session.add(new_user)
        al_db.db_session.commit()
        session['user_id'] = new_user.id
        session['user_name'] = new_user.name
        return redirect(url_for('login'))
    return render_template('registration.html')


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        user_login = request.form.get('login')
        user_password = request.form.get('password')
        user = al_db.db_session.query(User).filter_by(login=user_login).first()
        if user is None:
            flash('User is not found')
            return redirect(url_for('login'))
        if user.password != user_password:
            flash('Wrong password! Please, try again')
            return redirect(url_for('login'))
        else:
            session['user_id'] = user.id
            session['user_name'] = user.name
            return redirect(url_for('main_page'))
    return render_template('login.html')


@app.route('/logout/', methods=['GET'])
def logout():
    session.pop('user_id', None)
    session.pop('user_name', None)
    return redirect(url_for('main_page'))


@app.route('/vacancy/', methods=['GET', 'POST'])
def vacancy():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    al_db.init_db()
    mongo = mongo_db.MongoDB()
    if request.method == 'POST':
        position_name = request.form.get('position_name')
        company = request.form.get('company')
        description = request.form.get('description')
        contact_name = request.form.get('contact_name')
        contact_email = request.form.get('contact_email')
        contact_phone = request.form.get('contact_phone')
        comment = request.form.get('comment')

        contact_id = mongo.insert_contact({"name": contact_name, "email": contact_email, "phone":
            contact_phone})

        all_vacancies = Vacancy(company, str(contact_id), description, position_name, comment, status=1,
                                user_id=session.get('user_id'))
        al_db.db_session.add(all_vacancies)
        al_db.db_session.commit()

    result = al_db.db_session.query(Vacancy.id, Vacancy.position_name, Vacancy.contacts_ids, Vacancy.company,
        Vacancy.description, Vacancy.comment). filter_by(user_id=session.get('user_id')).all()
    result_data = []
    for item in result:
        contacts = item[2].split(',')
        contacts_result = []
        for one_contact in contacts:
            data = mongo.find_contact({'_id': ObjectId(one_contact)})
            contacts_result.append(data)
        result_data.append({'id': item[0], 'position_name': item[1], 'company': item[3], 'description': item[4],
                            'comment': item[5], 'contacts': contacts_result})
    return render_template('vacancy-add.html', vacancies=result_data)


@app.route('/vacancy/<int:vacancy_id>/', methods=['GET', 'POST'])
def up_vacancy(vacancy_id):
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    mongo = mongo_db.MongoDB()
    if request.method == 'POST':
        position_name = request.form.get('position_name')
        company = request.form.get('company')
        description = request.form.get('description')
        comment = request.form.get('comment')
        al_db.db_session.query(Vacancy).filter(Vacancy.id == vacancy_id).update({
            Vacancy.position_name: position_name, Vacancy.company: company, Vacancy.description: description,
             Vacancy.comment: comment})
        al_db.db_session.commit()

    result = al_db.db_session.query(Vacancy.id, Vacancy.position_name, Vacancy.company,
        Vacancy.description, Vacancy.comment, Vacancy.contacts_ids).where(Vacancy.id == vacancy_id).filter_by(
        user_id=session.get('user_id')).all()

    for item in result:
        contacts = item[5].split(',')
        contacts_result = []
        for one_contact in contacts:
            data = mongo.find_contact({'_id': ObjectId(one_contact)})
            contacts_result.append(data)
        result_data = {'id': item[0], 'position_name': item[1], 'company': item[2], 'description': item[3],
                            'comment': item[4], 'contacts': contacts_result}

    return render_template('up_vacancy.html', vacancy_id=vacancy_id, result=result_data)


@app.route('/vacancy/<int:vacancy_id>/events/', methods=['GET', 'POST'])
def vacancy_events(vacancy_id):
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    if request.method == 'POST':
        description = request.form.get('description')
        title = request.form.get('title')
        due_to_date = request.form.get('due_to_date')

        all_events = Event(vacancy_id, description, title, due_to_date, status=1)
        al_db.db_session.add(all_events)
        al_db.db_session.commit()
    result = al_db.db_session.query(Event.id, Event.title, Event.description, Event.due_to_date). \
        where(Event.vacancy_id == vacancy_id)
    return render_template('event-add.html', vacancy_id=vacancy_id, events=result)


@app.route('/vacancy/<int:vacancy_id>/events/<int:event_id>/', methods=['GET', 'POST'])
def vacancy_event_id(vacancy_id, event_id):
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    if request.method == 'POST':
        description = request.form.get('description')
        title = request.form.get('title')
        due_to_date = request.form.get('due_to_date')
        al_db.db_session.query(Event).filter(Event.id == event_id, Event.vacancy_id == vacancy_id).update({
            Event.description: description, Event.title: title, Event.due_to_date: due_to_date})
        al_db.db_session.commit()
    event = al_db.db_session.query(Event.id, Event.title, Event.description, Event.due_to_date). \
        where(Event.id == event_id, Event.vacancy_id == vacancy_id).all()[0]
    return render_template('up_event.html', vacancy_id=vacancy_id, event_id=event_id, event=event)


@app.route('/vacancy/<int:vacancy_id>/history/', methods=['GET'])
def vacancy_history():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return 'Vacancy history'


@app.route('/user/', methods=['GET'])
def user():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return render_template('main_page.html', user_name=session.get('user_login'))


@app.route('/user/calendar/', methods=['GET'])
def user_calendar():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return 'User calendar'


@app.route('/user/mail/', methods=['GET', 'POST'])
def user_mail():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    email_settings = al_db.db_session.query(EmailCredentials).filter_by(user_id=session.get('user_id')).first()
    email_obj = email_lib.EmailWrapper(
        email_settings.login,
        email_settings.email,
        email_settings.password,
        email_settings.smtp_server,
        email_settings.smtp_port,
        email_settings.pop_server,
        email_settings.pop_port,
        email_settings.imap_server,
        email_settings.imap_port
    )
    if request.method == 'POST':
        recipient = request.form.get('recipient')
        message = request.form.get('message')
        subject = request.form.get('subject')
        send_email.apply_async(args=[email_settings.id, recipient, message, subject])
        return 'Your message was successfully sent!'
    if email_settings.imap_server and email_settings.imap_port:
        email_list = email_obj.get_imap()
    elif email_settings.pop_server and email_settings.pop_port:
        email_list = email_obj.get_pop3()
    else:
        email_list = None
    return render_template('send_email.html', emails=email_list)


@app.route('/user/settings/', methods=['GET', 'PUT'])
def user_settings():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return 'User settings'


@app.route('/user/files/', methods=['GET', 'POST', 'DELETE'])
def user_files():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return 'User files'


@app.route('/user/templates/', methods=['GET', 'POST', 'PUT', 'DELETE'])
def user_templates():
    if session.get('user_id', None) is None:
        return redirect(url_for('login'))
    return 'User templates'


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5005, debug=True)
