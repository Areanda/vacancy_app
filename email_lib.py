import imaplib
import poplib
import smtplib
import ssl
from email.header import decode_header
import email


class EmailWrapper:
    def __init__(self, user_email, login, password, smtp_server, smtp_port=465, pop_server=None, pop_port=995,
                 imap_server=None, imap_port=993):
        self.user_email = user_email
        self.login = login
        self.password = password
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.pop_server = pop_server
        self.pop_port = pop_port
        self.imap_server = imap_server
        self.imap_port = imap_port

    def send_email(self, recipient, message, subject):
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.smtp_server, self.smtp_port, context=context) as server:
            server.login(self.user_email, self.password)
            server.sendmail(self.user_email, recipient, message, subject)

    def get_emails(self, protocol='imap'):
        if protocol == 'pop3':
            return self.get_pop3()
        if protocol == 'imap':
            return self.get_imap()
        else:
            raise ValueError('unknown protocol')

    def get_pop3(self):
        m = poplib.POP3_SSL(self.pop_server, self.pop_server)
        m.user(self.user_email)
        m.pass_(self.password)
        num_messages = len(m.list()[1])
        start = num_messages - 4
        end = num_messages
        message_ids = range(start, end + 1)
        messages = []
        for message_id in message_ids:
            raw_message = b'\n'.join(m.retr(message_id)[1])
            message = email.message_from_bytes(raw_message)
            date = message.get('Date')
            subject = message.get('Subject')
            sender = message.get('From')
            if subject:
                subject = decode_header(subject)[0][0]
                if isinstance(subject, bytes):
                    subject = subject.decode()
            if sender:
                sender = decode_header(sender)[0][0]
                if isinstance(sender, bytes):
                    sender = sender.decode()
            message_dict = {'date': date, 'subject': subject, 'sender': sender}
            messages.append(message_dict)
        m.quit()
        return messages[::-1]

    def get_imap(self):
        m = imaplib.IMAP4_SSL(self.imap_server, self.imap_port)
        m.login(self.user_email, self.password)
        m.select('INBOX')
        typ, data = m.search(None, 'ALL')
        message_ids = data[0].split()[-5:]
        messages = []
        for message_id in message_ids:
            typ, data = m.fetch(message_id, "(RFC822)")
            message = email.message_from_bytes(data[0][1])
            date = message.get('Date')
            subject = message.get('Subject')
            sender = message.get('From')
            if subject:
                subject = decode_header(subject)[0][0]
                if isinstance(subject, bytes):
                    subject = subject.decode()
            if sender:
                sender = decode_header(sender)[0][0]
                if isinstance(sender, bytes):
                    sender = sender.decode()
            message_dict = {'date': date, 'subject': subject, 'sender': sender}
            messages.append(message_dict)
        m.close()
        m.logout()
        return messages[::-1]


