import os
import pymongo

MONGO_HOST = os.environ.get('MONGO_HOST', 'localhost')


class MongoDB:

    def __init__(self):
        self.client = pymongo.MongoClient(f'mongodb://root:example@{MONGO_HOST}:27017/')
        self.database = self.client["vacancy_db"]
        self.collection = self.database["contacts"]

    def insert_contact(self, new_contact: dict):
        contact_id = self.collection.insert_one(new_contact).inserted_id
        return contact_id

    def find_contact(self, one_contact):
        data = self.collection.find_one(one_contact)
        return data
