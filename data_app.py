vacancy_data = [
    {
         'id': 1,
         'creation_date': '01.02.2023',
         'status': 1,
         'company': 'company 1',
         'contacts_ids': [1, 2],
         'description': 'Vacancy description',
         'position_name': 'Junior Python Developer',
         'comment': 'My comments',
         'user_id': 1
    },
    {
         'id': 2,
         'creation_date': '27.01.2023',
         'status': 1,
         'company': 'company 2',
         'contacts_ids': [3],
         'description': 'Vacancy description',
         'position_name': 'Strong Junior/Middle Python Developer',
         'comment': 'My comments',
         'user_id': 1
    },
    {
         'id': 3,
         'creation_date': '02.02.2023',
         'status': 1,
         'company': 'company 3',
         'contacts_ids': [4, 5],
         'description': 'Vacancy description',
         'position_name': 'Junior Odoo Developer',
         'comment': 'My comments',
         'user_id': 1
    }
]


events_data = [
    {
        'id': 1,
        'vacancy_id': 1,
        'description': 'Description 1',
        'event_date': '01.02.2023',
        'title': 'Event title',
        'due_to_date': '03.02.2023',
        'status': 1
    },
    {
        'id': 2,
        'vacancy_id': 3,
        'description': 'Description 2',
        'event_date': '27.01.2023',
        'title': 'Event title',
        'due_to_date': '01.02.2023',
        'status': 1
    },
    {
        'id': 3,
        'vacancy_id': 3,
        'description': 'Description 3',
        'event_date': '02.02.2023',
        'title': 'Event title',
        'due_to_date': '06.02.2023',
        'status': 1
    }
]